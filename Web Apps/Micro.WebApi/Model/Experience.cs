﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Micro.WebApi.Model
{
    public class Experience
    {
        public string Title { get; set; }

        public string Company { get; set; }

        public string Location { get; set; }

        public List<Project> Projects { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }
    }
}
