﻿using System.Collections.Generic;

namespace Micro.WebApi.Model
{
    public class Project
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public List<string> Tasks { get; set; }

        public List<string> Environments { get; set; }
    }
}