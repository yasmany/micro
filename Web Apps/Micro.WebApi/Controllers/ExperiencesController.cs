﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Micro.WebApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace Micro.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ExperiencesController : Controller
    {
        private List<Experience> xps = new List<Experience>()
        {
            new Experience() {
                Title = "Sr .Net Developer / Js Developer",
                Company = "Oceanwide", 
                Location = "Montreal, Canada", 
                From = new DateTime(2016, 1, 1), 
                Projects = new List<Project>() {
                    new Project() {
                        Title = "SmartView application.",
                        Description = "Building a Business Intelligence application that allows users to work with data coming from external systems. Conduct meaningful analysis and create interactive visual analytics in the form of dashboards.", 
                        Tasks = new List<string>() {
                            "Development of Globalization and Notifications processes as microservices to be used for different applications.",
                            "Development of a Template Editor using the javascript library CodeMirror.",
                            "Using of BDD and TDD for automation tests.",
                            "Use of SignalR to have real-time communication between server-client and update metadata.",
                            "Addition of Docker in the deployment process.",
                            "Use of Kendo UI Components as user interface framework for building SmartView."
                        }
                    }, 
                    new Project() {
                        Title = "UX Framework.",
                        Description = "Building a framework that empowers teams to build better and faster products UI.", 
                        Tasks = new List<string>() {
                            "Development of different UI controls using React.js (Grid, Multiselect, Switch, ProgressBar, etc)."
                        },
                        Environments = new List<string>() {
                            "Javascript, Typescript, Node.js, React.js, Docker, HTML5, CSS3, Less, Kendo UI, C#, ASP.NET WebApi, Visual Studio 2017, TFS, .NET Framework 3.5, 4.0, 4.5, WinForm, SQL Server 2012, SQL Server Reporting Services, Jenkins, nUnit, SpecFlow, Git."
                        }
                    }
                }
            },
            new Experience() {
                Title = "CGI Consultant - Sr .Net Developer", 
                Company = "Societe Generale", 
                Location = "Montreal, Canada", 
                From = new DateTime(2014, 7, 1), 
                To = new DateTime(2015, 12, 31), 
                Projects = new List<Project>() {
                    new Project() {
                        Title = "Merlin",
                        Description = "Software developer on Merlin, a Hedge Fund risk management application.",
                        Tasks = new List<string>() {
                            "Maintain and develop new features for a Winform application which monitor collateral for financial products for a deal. It monitors Bonds, Stock, Hedge Funds, and Mutual Funds as collateral.",
                            "Develop a reporting system to feed a data warehouse and generate reports.",
                            "Using of BDD and TDD for automation tests. ",
                            "Using Scrum and Agile practices."
                        }, 
                        Environments = new List<string>() {
                            "Visual Studio 2013, Jira, FishEye, Subversion, .NET Framework 3.5, 4.0, 4.5, C#, WinForm, WPF, Entity Framework, SQL Server 2012, SQL Server Reporting Services, Jenkins, Nexus, DeployIt, TMon, Tibco, nUnit, SpecFlow, ASP.NET WebApi, Angular JS."
                        }
                    }
                }
            },
            new Experience() {
                Title = ".Net Developer",
                Company = "CGI",
                Location = "Montreal, Canada",
                From = new DateTime(2014, 5, 1),
                To = new DateTime(2014, 7, 1),
                Projects = new List<Project>() {
                    new Project() {
                        Title = "Morneau Shepell Project",
                        Description = "Worked as a .NET consultant in the development of architectural components in a new project for Morneau Shepell.",
                        Tasks = new List<string>() {
                            "Integrated the site security with SAML 2.0.",
                            "Worked in the internationalization of the site.",
                            "Implemented site security, based on functionality.",
                            "Trained all staff on technologies to use in the project and developed easy-to-use, but functionally data integrations and data caching.",
                            "Worked on specific modules of the business logic project."
                        }, 
                        Environments = new List<string>() {
                            "Visual Studio 2013, Team Foundation Server, .NET Framework 4.0, C#, ASP.NET MVC 5, Entity Framework, JavaScript, HTML5, CSS3, jQuery, SQL Server 2012."
                        }
                    }
                }
            },
            new Experience() {
                Title = "Team Lead for ASP.NET MVC Development",
                Company = "Merlot Aero Limited", 
                Location = "Auckland, New Zealand",
                From = new DateTime(2010, 9, 1),
                To = new DateTime(2014, 1, 1),
                Projects = new List<Project>() {
                    new Project() {
                        Title = "CrewPortal and AircraftPortal", 
                        Description = "Development of web applications for the aviation industry. The applications were dedicated to the management of business processes, personnel management, flight management, internal and external reporting.",
                        Tasks = new List<string>() {
                            "Participated in the development, implementation, and maintenance of company website (www.merlot.aero) and portals CrewPortal and AircraftPortal.",
                            "Write, modify, integrate and test software code according to the requirements of the client.",
                            "Provide advice on information systems strategy, policy, management, security and service delivery.",
                            "Source, select and organize information for inclusion and design the appearance, layout, and flow of the Web site.",
                            "Assist in the development of logical and physical specifications."
                        },
                        Environments = new List<string>() {
                            "Visual Studio 2012, .NET Framework 3.5, 4.0, C#, ASP.NET MVC 3, Entity Framework, Dynamic Data, CMS Orchard, Silverlight 5, Prism, JavaScript, Web Services, SQL Server 2008 R2, SQL Server Reporting Services, Subversion, IIS, Jira."
                        }
                    }
                }
            }
        };

        // GET api/values
        [HttpGet]
        public IEnumerable<Experience> Get()
        {
            return xps;
        }
    }
}
