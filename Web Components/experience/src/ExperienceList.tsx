﻿import * as React from 'react';
import { Description } from 'src/Description';
import { Experience } from 'src/Experience';

export interface IExperienceListProps {
    items: Experience[];
    selectedYear?: number;
}

export interface IExperienceListState {
    experience?: Experience;
}

export class ExperienceList extends React.Component<IExperienceListProps, any> {
    public readonly state: IExperienceListState = { experience: undefined };
    

    constructor(props: IExperienceListProps) {
        super(props);

        this.onMouseOverExperience = this.onMouseOverExperience.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
    }

    public onMouseOverExperience(experience: Experience) {
        this.setState({ experience });
    }

    public onMouseLeave() {
        this.setState({ experience: undefined });
    }

    public render() {
        let selectedYear = new Date().getFullYear();
        if (this.props.selectedYear) {
            selectedYear = this.props.selectedYear;
        }
        return (
            <div className="xp-container">
                <ul className="xp-summaries">
                    {this.props.items.filter(item => item.from.getFullYear() === selectedYear || ((item.to && item.to.getFullYear() || new Date().getFullYear()) === selectedYear)).map((experience, index) => {
                        return (
                            <li className="xp-summary" key={index} onMouseOver={this.onMouseOverExperience.bind(this, experience)} onMouseLeave={this.onMouseLeave}>
                                <div className="title">{experience.title}</div>
                                <div>{experience.company}</div>
                                <div>{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short' }).format(experience.from)} - {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short' }).format(experience.to)}</div>
                                <div>{experience.location}</div>
                            </li>
                        );
                    })}
                </ul>
                <Description experience={this.state.experience} />
            </div>
        );
    }
}
