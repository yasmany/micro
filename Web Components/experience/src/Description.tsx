﻿import * as React from 'react';
import { Experience } from 'src/Experience';

export interface IDescriptionProps {
    experience?: Experience;
}

export class Description extends React.Component<IDescriptionProps, any> {
    constructor(props: IDescriptionProps) {
        super(props);
    }

    public render() {
        if (this.props.experience) {
            return (
                <div className="xp-long-desc">
                    <ul>
                        {this.props.experience.projects.map((project, index) => {
                            return (
                                <li className="xp-project" key={index}>
                                    <div className="xp-title">{project.title}</div>
                                    <div className="xp-description">{project.description}</div>
                                    <ul>
                                        {project.tasks.map((task, index2) => {
                                            return (
                                                <li className="xp-task" key={index2}>{task}</li>
                                            );
                                        })}
                                    </ul>
                                    {project.environments ? <div className="tech-label">Technical Environments</div> : null}
                                    <ul>
                                        {project.environments && project.environments.map((environment, index3) => {
                                            return (
                                                <li className="xp-env" key={index3}>{environment}</li>
                                            );
                                        })}
                                    </ul>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            );
        }

        return null;
    }
}
