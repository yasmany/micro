﻿import * as React from 'react';
import { render } from 'react-dom';
import App from './App';

window.customElements.define('app-xp', class ExperienceApp extends HTMLElement {
    public connectedCallback() {
        render(<App />, this);
    }
});