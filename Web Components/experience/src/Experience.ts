﻿import { Project } from "src/Project";

export class Experience {
    constructor(public title: string, public company: string, public location: string, public projects: Project[], public from: Date, public to?: Date) { }
}