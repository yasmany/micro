import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import * as React from 'react';
import { Experience } from 'src/Experience';
import { ExperienceList } from 'src/ExperienceList';
import 'src/index.css';

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/;

function reviver(key: any, value: any) {
    if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
    }
    if (key === "to" && !value) {
        return new Date();
    }
    return value;
}

export interface IAppState {
    selectedYear?: number;
    experiences?: Experience[];
}

export class App extends React.Component<any, IAppState> {
    
    public readonly state: IAppState = {
        experiences: undefined,
        selectedYear: new Date().getFullYear()
    };
    
    public constructor(props: any) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    public componentDidMount() {
        fetch("http://localhost:64337/api/experiences")
            .then(results => results.text())
            .then(text => JSON.parse(text, reviver))
            .then(data => {
                this.setState({ experiences: data });
            });
    }

    public render() {
        const experiences = this.state.experiences;
        if (experiences) {
            let init = 10000;
            const end = new Date().getFullYear();
            const marks = {};
            experiences.forEach((experience) => {
                const fromYear = new Date(experience.from).getFullYear();
                const toYear = experience.to != null ? new Date(experience.to).getFullYear() : end;
                if (fromYear < init) {
                    init = fromYear;
                }
                marks[fromYear] = fromYear;
                marks[toYear] = toYear;
            });

            return (
                <div className="xps-container">
                    <div className="xps-slider-container">
                        <Slider vertical={true} min={init} max={end} marks={marks} step={null} defaultValue={end} onChange={this.onChange} />
                    </div>
                    <ExperienceList items={experiences} selectedYear={this.state.selectedYear} />
                </div>
            );
        }

        return null;
    }

    public onChange(value: any) {
        this.setState({ selectedYear: value });
    }
}

export default App;
