﻿export class Project {
    constructor(public title: string, public description: string, public tasks: string[], public environments: string[]) { }
}