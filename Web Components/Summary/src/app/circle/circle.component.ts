import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'circle',
  templateUrl: './circle.component.html',
  styleUrls: ['../shared/shared.styles.css', './circle.component.css']
})
export class CircleComponent implements OnInit {

  @Input() x: number;
  @Input() y: number;
  @Input() radio: number;
  @Input() level: number;
  @Input() text: string;
  @Output() clickCircle = new EventEmitter();

  ngOnInit() {
  }

  onClickCircle(event) {
    event.stopPropagation();
    this.clickCircle.emit();
  }
}
