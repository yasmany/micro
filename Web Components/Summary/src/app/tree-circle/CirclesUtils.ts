import { Node } from './Node';
import { TreeNode } from 'src/app/tree-circle/TreeNode';

export class CirclesUtils {

  public static toDegree(radians: number) {
    return radians * (180 / Math.PI);
  }

  public static roundNumber(number: number, decimals?: number) {
    decimals = decimals || 2;
    return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
  }

  public static asin(number: number) {
    return CirclesUtils.roundNumber(Math.sin(number * Math.PI / 180.0));
  }
  public static acos(number: number) {
    return CirclesUtils.roundNumber(Math.cos(number * Math.PI / 180.0));
  }

  public static GetNodes(size: number, current: TreeNode): Node[] {
    var radio;
    var count = current.children.length;
    if (count === 0) return [];
    if (count === 1) return [new Node(size / 2, size / 2, size / 2 * 0.6, current.children[0])];

    var angle = 0;
    var result: Node[] = [];
    var firstx, firsty;
    for (var i = 0; i < count; i++) {
      var x = size / 4 * CirclesUtils.acos(angle) + size / 2;
      var y = size / 4 * CirclesUtils.asin(angle) + size / 2;

      if (i === 0) {
        firstx = x;
        firsty = y;
      } else {
        if (i === 1) {
          radio = CirclesUtils.roundNumber(Math.sqrt(Math.pow(firstx - x, 2) + Math.pow(firsty - y, 2)) / 2) - 5;
          result.push(new Node(firstx, firsty, radio, current.children[0]));
          result.push(new Node(x, y, radio, current.children[i]));
        } else {
          result.push(new Node(x, y, radio, current.children[i]));
        }
      }
      
      
      angle += (360 / count);
    }
    return result;
  }
}
