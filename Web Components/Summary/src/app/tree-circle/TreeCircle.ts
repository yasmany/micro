import { TreeNode } from "src/app/tree-circle/TreeNode";

export class TreeCircle {
  public current: TreeNode;

  constructor(public root: TreeNode) {
    this.current = root;
  }
}
