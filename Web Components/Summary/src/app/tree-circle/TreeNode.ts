export class TreeNode {
  constructor(public info: any, public parent?: TreeNode, public children?: TreeNode[]) {
    if (this.children == null) {
      this.children = [];
    }
  }
}
