import { TreeNode } from "src/app/tree-circle/TreeNode";

export class Node {
  private x: number
  private y: number;
  private radio: number;

  constructor(x: number, y: number, radio: number, public node: TreeNode) {
    this.x = x - radio;
    this.y = y - radio;
    this.radio = 2 * radio;
  }


}
