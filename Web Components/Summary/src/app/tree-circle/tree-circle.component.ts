import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { TreeCircle } from 'src/app/tree-circle/TreeCircle';
import { TreeNode } from 'src/app/tree-circle/TreeNode';
import { CirclesUtils } from 'src/app/tree-circle/CirclesUtils';
import { Node } from 'src/app/tree-circle/Node';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'tree-circle',
  templateUrl: './tree-circle.component.html',
  styleUrls: ['../shared/shared.styles.css', './tree-circle.component.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class TreeCircleComponent implements OnInit {

  @Input() size: number;
  public tree: TreeCircle;
  private level: number = 0;
  private nodes: Node[];

  constructor(private cdr: ChangeDetectorRef) {
    var root = new TreeNode('Root');
    root.children.push(new TreeNode('Summary', root));
    root.children.push(new TreeNode('Experience', root));
    root.children[1].children.push(new TreeNode('Oceanwide', root.children[1]));
    root.children[1].children.push(new TreeNode('Societe Generale', root.children[1]));
    root.children[1].children.push(new TreeNode('CGI', root.children[1]));
    this.tree = new TreeCircle(root);
  }

  ngOnInit() {
    this.nodes = CirclesUtils.GetNodes(+this.size, this.tree.current);
  }

  onClick(event) {
    var p = this.tree.current.parent;
    if (p != null) {
      this.tree.current = null;
      this.cdr.detectChanges();
      this.level--;
      this.nodes = CirclesUtils.GetNodes(+this.size, p);
      this.tree.current = p;
      this.cdr.detectChanges();
    }
  }

  onClickNode(n: Node) {
    this.tree.current = null;
    this.cdr.detectChanges();
    this.level++;
    this.nodes = CirclesUtils.GetNodes(+this.size, n.node);
    this.tree.current = n.node;
    this.cdr.detectChanges();
  }
}
