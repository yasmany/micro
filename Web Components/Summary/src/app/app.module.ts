import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { createCustomElement } from '@angular/elements'

import { CircleComponent } from './circle/circle.component';
import { TreeCircleComponent } from './tree-circle/tree-circle.component';

@NgModule({
  declarations: [
    CircleComponent,
    TreeCircleComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule
  ],
  entryComponents: [
    TreeCircleComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector) {
    const c1 = createCustomElement(TreeCircleComponent, { injector });
    
    customElements.define('app-tree-circle', c1);
  }

  ngDoBootstrap() { }
}
